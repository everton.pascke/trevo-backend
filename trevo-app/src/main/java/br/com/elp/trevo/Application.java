package br.com.elp.trevo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan
@EnableScheduling
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        builder.beanNameGenerator((definition, registry) -> {
            String beanClassName = definition.getBeanClassName();
            return beanClassName;
        });
        return builder.sources(getClass());
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new br.com.elp.trevo.utility.jackson.ObjectMapper();
    }

}
