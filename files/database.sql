/*

docker run \
--name postgres \
-p 5432:5432 \
-e POSTGRES_PASSWORD=postgres \
-v /Users/elpascke/Develop/Docker/PostgreSQL/data:/var/lib/postgresql/data \
-d postgres:9.6.9

docker exec -it postgres /bin/bash

su postgres
psql

create role trevo with login superuser password 'trevo';
create database trevo with owner = trevo encoding = 'UTF8';

\q
psql -d trevo -h localhost -U postgres

create extension pgcrypto;

\q

psql -d trevo -h localhost -U trevo

\i /var/lib/postgresql/data/scripts/<file>.sql
*/

drop table if exists rodada;
drop table if exists ganhador;
drop table if exists regiao;
drop table if exists mega_sena;
drop table if exists lotofacil;
drop table if exists quina;
drop table if exists lotomania;
drop table if exists timemania;
drop table if exists dupla_sena;
drop table if exists sorteio;
drop table if exists concurso;

create table concurso (
  id bigserial not null,
  tipo character varying(20) not null,
  data date not null,
  numero bigint not null,
  arrecadacao numeric(15, 2) not null,
  constraint concurso_pk primary key (id)
);

create table regiao (
  id bigserial not null,
  concurso_id bigint null,
  uf character varying(2) not null,
  cidade text null,
  constraint regiao_pk primary key (id),
  constraint regiao_concurso_fk foreign key (concurso_id) references concurso (id)
);

create table sorteio (
  id bigserial not null,
  concurso_id bigint null,
  ordem integer not null,
  constraint sorteio_pk primary key (id),
  constraint sorteio_concurso_fk foreign key (concurso_id) references concurso (id)
);

create table rodada (
  id bigserial not null,
  sorteio_id bigint null,
  ordem integer not null,
  numero text not null,
  constraint rodada_pk primary key (id),
  constraint rodada_sorteio_fk foreign key (sorteio_id) references sorteio (id)
);

create table ganhador (
  id bigserial not null,
  sorteio_id bigint null,
  rateio numeric(15, 2) not null,
  acertos integer not null,
  quantidade integer not null,
  constraint ganhador_pk primary key (id),
  constraint ganhador_sorteio_fk foreign key (sorteio_id) references sorteio (id)
);


create table mega_sena (
  id bigint not null,
  acumulado bool not null,
  valor_acumulado numeric(15, 2) not null,
  estimativa_premio numeric(15, 2) not null,
  acumulado_mega_virada numeric(15, 2) not null,
  constraint mega_sena_pk primary key (id),
  constraint mega_sena_concurso_fk foreign key (id) references concurso (id)
);

create table lotofacil (
   id bigint not null,
   estimativa_premio numeric(15, 2) not null,
   acumulado_15_numeros numeric(15, 2) not null,
   valor_acumulado_especial numeric(15, 2) not null,
   constraint lotofacil_pk primary key (id),
   constraint lotofacil_concurso_fk foreign key (id) references concurso (id)
);

create table quina (
   id bigint not null,
   acumulado bool not null,
   valor_acumulado numeric(15, 2) not null,
   estimativa_premio numeric(15, 2) not null,
   valor_acumulado_sorteio_especial_sao_joao numeric(15, 2) not null,
   constraint quina_pk primary key (id),
   constraint quina_concurso_fk foreign key (id) references concurso (id)
);

create table lotomania (
   id bigint not null,
   acumulado_0_numeros numeric(15, 2) not null,
   acumulado_16_numeros numeric(15, 2) not null,
   acumulado_17_numeros numeric(15, 2) not null,
   acumulado_18_numeros numeric(15, 2) not null,
   acumulado_19_numeros numeric(15, 2) not null,
   acumulado_20_numeros numeric(15, 2) not null,
   estimativa_premio numeric(15, 2) not null,
   valor_acumulado_especial numeric(15, 2) not null,
   constraint lotomania_pk primary key (id),
   constraint lotomania_concurso_fk foreign key (id) references concurso (id)
);

create table timemania (
   id bigint not null,
   time_coracao text not null,
   valor_acumulado numeric(15, 2) not null,
   estimativa_premio numeric(15, 2) not null,
   constraint timemania_pk primary key (id),
   constraint timemania_concurso_fk foreign key (id) references concurso (id)
);

create table dupla_sena (
   id bigint not null,
   acumulado_sena_sorteio_1 bool not null,
   valor_acumulado_sena_sorteio_1 numeric(15, 2) not null,
   estimativa_premio numeric(15, 2) not null,
   acumulado_especial_pascoa numeric(15, 2) not null,
   constraint dupla_sena_pk primary key (id),
   constraint dupla_sena_concurso_fk foreign key (id) references concurso (id)
);