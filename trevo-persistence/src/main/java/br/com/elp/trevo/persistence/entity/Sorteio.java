package br.com.elp.trevo.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sorteio")
public class Sorteio extends Identity {

    private Integer ordem;

    private Concurso concurso;
    private List<Rodada> rodadas;
    private List<Ganhador> ganhadores;

    public Sorteio() {
    }

    public Sorteio(Integer ordem) {
        this.ordem = ordem;
    }

    public void addRodada(Integer ordem, String numero) {
        if (rodadas == null) {
            rodadas = new ArrayList<>();
        }
        rodadas.add(new Rodada(ordem, numero));
    }

    public void addGanhador(Double rateio, Integer acertos, Integer quantidade) {
        if (ganhadores == null) {
            ganhadores = new ArrayList<>();
        }
        ganhadores.add(new Ganhador(rateio, acertos, quantidade));
    }

    @Column(name = "ordem", nullable = false)
    public Integer getOrdem() {
        return ordem;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "concurso_id", nullable = false)
    public Concurso getConcurso() {
        return concurso;
    }

    @OneToMany(mappedBy = "sorteio", fetch = FetchType.LAZY)
    public List<Rodada> getRodadas() {
        return rodadas;
    }

    @OneToMany(mappedBy = "sorteio", fetch = FetchType.LAZY)
    public List<Ganhador> getGanhadores() {
        return ganhadores;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public void setRodadas(List<Rodada> rodadas) {
        this.rodadas = rodadas;
    }

    public void setGanhadores(List<Ganhador> ganhadores) {
        this.ganhadores = ganhadores;
    }
}
