package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "lotomania")
public class Lotomania extends Concurso {

    private Double acumulado0Numeros;
    private Double acumulado16Numeros;
    private Double acumulado17Numeros;
    private Double acumulado18Numeros;
    private Double acumulado19Numeros;
    private Double acumulado20Numeros;

    private Double estimativaPremio;
    private Double valorAcumuladoEspecial;

    public Lotomania() {
        super(Tipo.LOTOMANIA);
    }

    @Column(name = "acumulado_0_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado0Numeros() {
        return acumulado0Numeros;
    }

    @Column(name = "acumulado_16_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado16Numeros() {
        return acumulado16Numeros;
    }

    @Column(name = "acumulado_17_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado17Numeros() {
        return acumulado17Numeros;
    }

    @Column(name = "acumulado_18_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado18Numeros() {
        return acumulado18Numeros;
    }

    @Column(name = "acumulado_19_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado19Numeros() {
        return acumulado19Numeros;
    }

    @Column(name = "acumulado_20_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado20Numeros() {
        return acumulado20Numeros;
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    @Column(name = "valor_acumulado_especial", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumuladoEspecial() {
        return valorAcumuladoEspecial;
    }

    public void setAcumulado0Numeros(Double acumulado0Numeros) {
        this.acumulado0Numeros = acumulado0Numeros;
    }

    public void setAcumulado16Numeros(Double acumulado16Numeros) {
        this.acumulado16Numeros = acumulado16Numeros;
    }

    public void setAcumulado17Numeros(Double acumulado17Numeros) {
        this.acumulado17Numeros = acumulado17Numeros;
    }

    public void setAcumulado18Numeros(Double acumulado18Numeros) {
        this.acumulado18Numeros = acumulado18Numeros;
    }

    public void setAcumulado19Numeros(Double acumulado19Numeros) {
        this.acumulado19Numeros = acumulado19Numeros;
    }

    public void setAcumulado20Numeros(Double acumulado20Numeros) {
        this.acumulado20Numeros = acumulado20Numeros;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public void setValorAcumuladoEspecial(Double valorAcumuladoEspecial) {
        this.valorAcumuladoEspecial = valorAcumuladoEspecial;
    }
}
