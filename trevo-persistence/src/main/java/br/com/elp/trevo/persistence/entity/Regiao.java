package br.com.elp.trevo.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "regiao")
public class Regiao extends Identity {

    private String uf;
    private String cidade;

    private Concurso concurso;

    public Regiao() {
    }

    public Regiao(String uf, String cidade) {
        this.uf = uf;
        this.cidade = cidade;
    }

    @Column(name = "uf", length = 2, nullable = false)
    public String getUf() {
        return uf;
    }

    @Column(name = "cidade")
    public String getCidade() {
        return cidade;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "concurso_id", nullable = false)
    public Concurso getConcurso() {
        return concurso;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }
}
