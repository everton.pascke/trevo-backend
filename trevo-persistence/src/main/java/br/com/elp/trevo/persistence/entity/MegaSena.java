package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "mega_sena")
public class MegaSena extends Concurso {

    private Boolean acumulado;

    private Double valorAcumulado;
    private Double estimativaPremio;
    private Double acumuladoMegaVirada;

    public MegaSena() {
        super(Tipo.MEGA_SENA);
    }

    @Column(name = "acumulado", nullable = false)
    public Boolean getAcumulado() {
        return acumulado;
    }

    @Column(name = "valor_acumulado", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumulado() {
        return valorAcumulado;
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    @Column(name = "acumulado_mega_virada", precision = 15, scale = 2, nullable = false)
    public Double getAcumuladoMegaVirada() {
        return acumuladoMegaVirada;
    }

    public void setAcumulado(Boolean acumulado) {
        this.acumulado = acumulado;
    }

    public void setValorAcumulado(Double valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public void setAcumuladoMegaVirada(Double acumuladoMegaVirada) {
        this.acumuladoMegaVirada = acumuladoMegaVirada;
    }
}
