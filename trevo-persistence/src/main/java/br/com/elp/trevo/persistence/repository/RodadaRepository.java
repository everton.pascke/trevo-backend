package br.com.elp.trevo.persistence.repository;

import br.com.elp.trevo.persistence.entity.Rodada;
import org.springframework.data.repository.CrudRepository;

public interface RodadaRepository extends CrudRepository<Rodada, Long> {

}
