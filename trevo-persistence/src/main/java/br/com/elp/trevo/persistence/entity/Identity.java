package br.com.elp.trevo.persistence.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

@MappedSuperclass
public abstract class Identity extends Entity<Long> implements Comparable<Identity> {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object other) {

        if (!getClass().isInstance(other)) {
            return false;
        }

        Long id = getId();
        if (id == null) {
            return super.equals(other);
        }

        Identity castOther = (Identity) other;
        Long castOtherId = castOther.getId();

        return new EqualsBuilder().append(id, castOtherId).isEquals();
    }

    @Override
    public int hashCode() {

        Long id = getId();

        if (id == null) {
            return super.hashCode();
        }

        return new HashCodeBuilder().append(id).toHashCode();
    }

    @Override
    public int compareTo(Identity o) {

        Long oId = o.getId();
        if (oId == null) {
            return 1;
        }

        Long thisId = getId();
        if (thisId == null) {
            return -1;
        }

        int compare = thisId.compareTo(oId);
        return compare;
    }
}
