package br.com.elp.trevo.persistence.repository;

import br.com.elp.trevo.persistence.entity.Ganhador;
import org.springframework.data.repository.CrudRepository;

public interface GanhadorRepository extends CrudRepository<Ganhador, Long> {
    
}
