package br.com.elp.trevo.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "rodada")
public class Rodada extends Identity {

    private Integer ordem;
    private String numero;

    private Sorteio sorteio;

    public Rodada() {
    }

    public Rodada(Integer ordem, String numero) {
        this.ordem = ordem;
        this.numero = numero;
    }

    @Column(name = "ordem", nullable = false)
    public Integer getOrdem() {
        return ordem;
    }

    @Column(name = "numero", nullable = false)
    public String getNumero() {
        return numero;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sorteio_id", nullable = false)
    public Sorteio getSorteio() {
        return sorteio;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setSorteio(Sorteio sorteio) {
        this.sorteio = sorteio;
    }
}
