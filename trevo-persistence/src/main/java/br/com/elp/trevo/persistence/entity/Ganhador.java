package br.com.elp.trevo.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "ganhador")
public class Ganhador extends Identity {

    private Double rateio;
    private Integer acertos;
    private Integer quantidade;

    private Sorteio sorteio;

    public Ganhador() {
    }

    public Ganhador(Double rateio, Integer acertos, Integer quantidade) {
        this.rateio = rateio;
        this.acertos = acertos;
        this.quantidade = quantidade;
    }

    @Column(name = "rateio", precision = 15, scale = 2, nullable = false)
    public Double getRateio() {
        return rateio;
    }

    @Column(name = "acertos", nullable = false)
    public Integer getAcertos() {
        return acertos;
    }

    @Column(name = "quantidade", nullable = false)
    public Integer getQuantidade() {
        return quantidade;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sorteio_id", nullable = false)
    public Sorteio getSorteio() {
        return sorteio;
    }

    public void setRateio(Double rateio) {
        this.rateio = rateio;
    }

    public void setAcertos(Integer acertos) {
        this.acertos = acertos;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public void setSorteio(Sorteio sorteio) {
        this.sorteio = sorteio;
    }
}
