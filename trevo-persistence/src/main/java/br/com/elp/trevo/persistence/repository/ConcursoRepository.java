package br.com.elp.trevo.persistence.repository;

import br.com.elp.trevo.persistence.entity.Concurso;
import org.springframework.data.repository.CrudRepository;

public interface ConcursoRepository extends CrudRepository<Concurso, Long> {

    Concurso findByTipoAndNumero(Concurso.Tipo tipo, Long numero);
    Concurso findFirstByTipoOrderByNumeroDesc(Concurso.Tipo tipo);
}
