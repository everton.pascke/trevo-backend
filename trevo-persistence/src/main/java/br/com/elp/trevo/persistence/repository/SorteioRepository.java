package br.com.elp.trevo.persistence.repository;

import br.com.elp.trevo.persistence.entity.Sorteio;
import org.springframework.data.repository.CrudRepository;

public interface SorteioRepository extends CrudRepository<Sorteio, Long> {

}
