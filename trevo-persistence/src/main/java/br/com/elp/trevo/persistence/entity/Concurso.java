package br.com.elp.trevo.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "concurso")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Concurso extends Identity {

    private Tipo tipo;
    private Date data;
    private Long numero;
    private Double arrecadacao;

    private List<Regiao> regioes;
    private List<Sorteio> sorteios;

    public Concurso(Tipo tipo) {
        this.tipo = tipo;
    }

    public enum Tipo {
        MEGA_SENA,
        LOTOFACIL,
        QUINA,
        LOTOMANIA,
        TIMEMANIA,
        DUPLA_SENA
    }

    public void addRegiao(String uf, String cidade) {
        if (regioes == null) {
            regioes = new ArrayList<>();
        }
        regioes.add(new Regiao(uf, cidade));
    }

    public void addSorteio(Sorteio sorteio) {
        if (sorteios == null) {
            sorteios = new ArrayList<>();
        }
        sorteios.add(sorteio);
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo", length = 20, nullable = false)
    public Tipo getTipo() {
        return tipo;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "data", nullable = false)
    public Date getData() {
        return data;
    }

    @Column(name = "numero", nullable = false)
    public Long getNumero() {
        return numero;
    }

    @Column(name = "arrecadacao", precision = 15, scale = 2, nullable = false)
    public Double getArrecadacao() {
        return arrecadacao;
    }

    @OneToMany(mappedBy = "concurso", fetch = FetchType.LAZY)
    public List<Regiao> getRegioes() {
        return regioes;
    }

    @OneToMany(mappedBy = "concurso", fetch = FetchType.LAZY)
    public List<Sorteio> getSorteios() {
        return sorteios;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public void setArrecadacao(Double arrecadacao) {
        this.arrecadacao = arrecadacao;
    }

    public void setRegioes(List<Regiao> regioes) {
        this.regioes = regioes;
    }

    public void setSorteios(List<Sorteio> sorteios) {
        this.sorteios = sorteios;
    }
}
