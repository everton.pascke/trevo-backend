package br.com.elp.trevo.persistence.repository;

import br.com.elp.trevo.persistence.entity.Regiao;
import org.springframework.data.repository.CrudRepository;

public interface RegiaoRepository extends CrudRepository<Regiao, Long> {

}
