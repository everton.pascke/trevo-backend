package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dupla_sena")
public class DuplaSena extends Concurso {

    private Boolean acumuladoSenaSorteio1;
    private Double valorAcumuladoSenaSorteio1;
    private Double estimativaPremio;
    private Double acumuladoEspecialPascoa;

    public DuplaSena() {
        super(Tipo.DUPLA_SENA);
    }

    @Column(name = "acumulado_sena_sorteio_1", nullable = false)
    public Boolean getAcumuladoSenaSorteio1() {
        return acumuladoSenaSorteio1;
    }

    @Column(name = "valor_acumulado_sena_sorteio_1", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumuladoSenaSorteio1() {
        return valorAcumuladoSenaSorteio1;
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    @Column(name = "acumulado_especial_pascoa", precision = 15, scale = 2, nullable = false)
    public Double getAcumuladoEspecialPascoa() {
        return acumuladoEspecialPascoa;
    }

    public void setAcumuladoSenaSorteio1(Boolean acumuladoSenaSorteio1) {
        this.acumuladoSenaSorteio1 = acumuladoSenaSorteio1;
    }

    public void setValorAcumuladoSenaSorteio1(Double valorAcumuladoSenaSorteio1) {
        this.valorAcumuladoSenaSorteio1 = valorAcumuladoSenaSorteio1;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public void setAcumuladoEspecialPascoa(Double acumuladoEspecialPascoa) {
        this.acumuladoEspecialPascoa = acumuladoEspecialPascoa;
    }
}
