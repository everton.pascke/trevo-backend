package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "quina")
public class Quina extends Concurso {

    private Boolean acumulado;

    private Double valorAcumulado;
    private Double estimativaPremio;
    private Double acumuladoSorteioEspecialSaoJoao;

    public Quina() {
        super(Tipo.QUINA);
    }

    @Column(name = "acumulado", nullable = false)
    public Boolean getAcumulado() {
        return acumulado;
    }

    @Column(name = "valor_acumulado", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumulado() {
        return valorAcumulado;
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    @Column(name = "valor_acumulado_sorteio_especial_sao_joao", precision = 15, scale = 2, nullable = false)
    public Double getAcumuladoSorteioEspecialSaoJoao() {
        return acumuladoSorteioEspecialSaoJoao;
    }

    public void setAcumulado(Boolean acumulado) {
        this.acumulado = acumulado;
    }

    public void setValorAcumulado(Double valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public void setAcumuladoSorteioEspecialSaoJoao(Double acumuladoSorteioEspecialSaoJoao) {
        this.acumuladoSorteioEspecialSaoJoao = acumuladoSorteioEspecialSaoJoao;
    }
}
