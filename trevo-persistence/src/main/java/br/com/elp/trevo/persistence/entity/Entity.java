package br.com.elp.trevo.persistence.entity;

import java.io.Serializable;

public abstract class Entity<ID extends Serializable> implements Serializable {

    protected ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
