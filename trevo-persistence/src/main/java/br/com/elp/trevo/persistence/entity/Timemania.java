package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "timemania")
public class Timemania extends Concurso {

    private String timeCoracao;

    private Double valorAcumulado;
    private Double estimativaPremio;

    public Timemania() {
        super(Tipo.TIMEMANIA);
    }

    @Column(name = "time_coracao", nullable = false)
    public String getTimeCoracao() {
        return timeCoracao;
    }

    @Column(name = "valor_acumulado", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumulado() {
        return valorAcumulado;
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    public void setTimeCoracao(String timeCoracao) {
        this.timeCoracao = timeCoracao;
    }

    public void setValorAcumulado(Double valorAcumulado) {
        this.valorAcumulado = valorAcumulado;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }
}
