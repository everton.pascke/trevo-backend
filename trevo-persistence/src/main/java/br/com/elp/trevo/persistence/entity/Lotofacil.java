package br.com.elp.trevo.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "lotofacil")
public class Lotofacil extends Concurso {

    private Double estimativaPremio;
    private Double acumulado15Numeros;
    private Double valorAcumuladoEspecial;

    public Lotofacil() {
        super(Tipo.LOTOFACIL);
    }

    @Column(name = "estimativa_premio", precision = 15, scale = 2, nullable = false)
    public Double getEstimativaPremio() {
        return estimativaPremio;
    }

    @Column(name = "acumulado_15_numeros", precision = 15, scale = 2, nullable = false)
    public Double getAcumulado15Numeros() {
        return acumulado15Numeros;
    }

    @Column(name = "valor_acumulado_especial", precision = 15, scale = 2, nullable = false)
    public Double getValorAcumuladoEspecial() {
        return valorAcumuladoEspecial;
    }

    public void setEstimativaPremio(Double estimativaPremio) {
        this.estimativaPremio = estimativaPremio;
    }

    public void setAcumulado15Numeros(Double acumulado15Numeros) {
        this.acumulado15Numeros = acumulado15Numeros;
    }

    public void setValorAcumuladoEspecial(Double valorAcumuladoEspecial) {
        this.valorAcumuladoEspecial = valorAcumuladoEspecial;
    }
}
