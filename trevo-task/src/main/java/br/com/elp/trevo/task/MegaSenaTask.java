package br.com.elp.trevo.task;

import br.com.elp.trevo.persistence.entity.MegaSena;
import br.com.elp.trevo.persistence.entity.Sorteio;
import br.com.elp.trevo.utility.TypeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MegaSenaTask extends ConcursoTask {

    @Value("${trevo.megasena.link}") private String link;
    @Value("${trevo.megasena.filename}") private String filename;

    //@Scheduled(cron = "0 0/5 * * * ?") // every 5 minutes
    public void run() {

        if (isDev()) {
            return;
        }

        byte[] bytes = download(link);

        unzip(bytes, link);
        process();
    }

    private void process() {

        try {
            Document document = Jsoup.parse(new File(filepath, filename), StandardCharsets.ISO_8859_1.name());
            List<MegaSena> concursos = extract(document);
            save(concursos);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<MegaSena> extract(Document document) {

        List<MegaSena> concursos = new ArrayList<>();

        Elements trs = document.select("table > tbody > tr");

        int row = 1;

        while (row < trs.size()) {

            MegaSena concurso = new MegaSena();

            Element tr = trs.get(row);
            Element td = tr.select("td").get(0);

            concurso.setNumero(TypeUtils.parse(Long.class, tr.select("td").get(0).text()));
            concurso.setData(TypeUtils.parse(Date.class, tr.select("td").get(1).text()));
            concurso.setArrecadacao(TypeUtils.parse(Double.class, tr.select("td").get(8).text()));

            int rowspan = Integer.parseInt(StringUtils.defaultIfBlank(td.attr("rowspan"), "1"));

            for (int i = row; i < (row + rowspan); i++) {

                String cidade = trs.get(i).select("td").get(i == row ? 10 : 0).text();
                String uf = trs.get(i).select("td").get(i == row ? 11 : 1).text();

                if (StringUtils.isNotBlank(uf)) {
                    concurso.addRegiao(StringUtils.trim(uf), StringUtils.trimToNull(cidade));
                }
            }

            concurso.setAcumulado(StringUtils.equalsIgnoreCase(tr.select("td").get(17).text(), "sim"));
            concurso.setValorAcumulado(TypeUtils.parse(Double.class, tr.select("td").get(18).text()));
            concurso.setEstimativaPremio(TypeUtils.parse(Double.class, tr.select("td").get(19).text()));
            concurso.setAcumuladoMegaVirada(TypeUtils.parse(Double.class, tr.select("td").get(20).text()));

            Sorteio sorteio = new Sorteio(1);

            for (int i = 0; i < 6; i++) {
                int ordem = i + 1;
                int index = i + 2;
                sorteio.addRodada(ordem, tr.select("td").get(index).text());
            }

            Double rateio;
            Integer quantidade;

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(9).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(12).text());
            sorteio.addGanhador(rateio, 6, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(13).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(14).text());
            sorteio.addGanhador(rateio, 5, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(15).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(16).text());
            sorteio.addGanhador(rateio, 4, quantidade);

            concurso.addSorteio(sorteio);
            concursos.add(concurso);

            row += rowspan;
        }

        return concursos;
    }
}
