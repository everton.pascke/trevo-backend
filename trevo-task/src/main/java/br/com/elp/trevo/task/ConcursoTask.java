package br.com.elp.trevo.task;

import br.com.elp.trevo.persistence.entity.*;
import br.com.elp.trevo.persistence.repository.ConcursoRepository;
import br.com.elp.trevo.service.GanhadorService;
import br.com.elp.trevo.service.RegiaoService;
import br.com.elp.trevo.service.RodadaService;
import br.com.elp.trevo.service.SorteioService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public abstract class ConcursoTask extends AbstractTask {

    @Value("${trevo.filepath}") protected String filepath;

    @Autowired private ConcursoRepository concursoRepository;

    @Autowired protected RodadaService rodadaService;
    @Autowired protected RegiaoService regiaoService;
    @Autowired protected SorteioService sorteioService;
    @Autowired protected GanhadorService ganhadorService;

    @Transactional(rollbackFor = Throwable.class)
    protected void save(List<? extends Concurso> concursos) {

        for (Concurso concurso : concursos) {

            concursoRepository.save(concurso);

            List<Regiao> regioes = concurso.getRegioes();
            if (CollectionUtils.isNotEmpty(regioes)) {
                for (Regiao regiao : regioes) {
                    regiao.setConcurso(concurso);
                    regiaoService.save(regiao);
                }
            }

            List<Sorteio> sorteios = concurso.getSorteios();
            for (Sorteio sorteio : sorteios) {

                sorteio.setConcurso(concurso);
                sorteioService.save(sorteio);

                List<Rodada> rodadas = sorteio.getRodadas();
                for (Rodada rodada : rodadas) {
                    rodada.setSorteio(sorteio);
                    rodadaService.save(rodada);
                }

                List<Ganhador> ganhadores = sorteio.getGanhadores();
                for (Ganhador ganhador : ganhadores) {
                    ganhador.setSorteio(sorteio);
                    ganhadorService.save(ganhador);
                }
            }
        }
    }

    protected byte[] download(String link) {

        CloseableHttpClient httpClient = HttpClients.custom()
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        try {

            ResponseEntity<byte[]> response = restTemplate.getForEntity(link, byte[].class);

            HttpStatus statusCode = response.getStatusCode();
            if (!HttpStatus.OK.equals(statusCode)) {
                throw new IllegalStateException("Falha ao obter resultado do link " + link + ". Status " + statusCode + ".");
            }

            return response.getBody();

        } catch (RestClientException e) {
            throw new IllegalStateException(e);
        }
    }

    protected void unzip(byte[] bytes, String zipLink) {

        File file = new File(filepath, FilenameUtils.getName(zipLink));
        ZipFile zipFile = null;

        try {

            FileUtils.writeByteArrayToFile(file, bytes);
            zipFile = new ZipFile(file);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            if (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                File destination = new File(filepath, entry.getName());
                if (entry.isDirectory()) {
                    destination.mkdirs();
                } else {
                    destination.getParentFile().mkdirs();
                    InputStream input = zipFile.getInputStream(entry);
                    FileOutputStream output = new FileOutputStream(destination);
                    IOUtils.copy(input, output);
                    IOUtils.closeQuietly(input);
                    output.close();
                }
            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
