package br.com.elp.trevo.task;

import br.com.elp.trevo.persistence.entity.Lotomania;
import br.com.elp.trevo.persistence.entity.Sorteio;
import br.com.elp.trevo.utility.TypeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class LotomaniaTask extends ConcursoTask {

    @Value("${trevo.lotomania.link}") private String link;
    @Value("${trevo.lotomania.filename}") private String filename;

    //@Scheduled(cron = "0 0/5 * * * ?") // every 5 minutes
    public void run() {

        System.out.println("Task iniciado.");

        if (isDev()) {
            return;
        }

        byte[] bytes = download(link);

        unzip(bytes, link);
        process();

        System.out.println("Task concluido.");
    }

    private void process() {

        try {
            Document document = Jsoup.parse(new File(filepath, filename), StandardCharsets.ISO_8859_1.name());
            List<Lotomania> concursos = extract(document);
            save(concursos);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Lotomania> extract(Document document) {

        List<Lotomania> concursos = new ArrayList<>();

        Elements trs = document.select("table > tbody > tr");

        int row = 1;

        while (row < trs.size()) {

            Lotomania concurso = new Lotomania();

            Element tr = trs.get(row);
            Element td = tr.select("td").get(0);

            concurso.setNumero(TypeUtils.parse(Long.class, tr.select("td").get(0).text()));
            concurso.setData(TypeUtils.parse(Date.class, tr.select("td").get(1).text()));
            concurso.setArrecadacao(TypeUtils.parse(Double.class, tr.select("td").get(22).text()));

            int rowspan = Integer.parseInt(StringUtils.defaultIfBlank(td.attr("rowspan"), "1"));

            for (int i = row; i < (row + rowspan); i++) {

                String cidade = trs.get(i).select("td").get(i == row ? 24 : 0).text();
                String uf = trs.get(i).select("td").get(i == row ? 25 : 1).text();

                if (StringUtils.isNotBlank(uf)) {
                    concurso.addRegiao(StringUtils.trim(uf), StringUtils.trimToNull(cidade));
                }
            }

            concurso.setAcumulado20Numeros(TypeUtils.parse(Double.class, tr.select("td").get(37).text()));
            concurso.setAcumulado19Numeros(TypeUtils.parse(Double.class, tr.select("td").get(38).text()));
            concurso.setAcumulado18Numeros(TypeUtils.parse(Double.class, tr.select("td").get(39).text()));
            concurso.setAcumulado17Numeros(TypeUtils.parse(Double.class, tr.select("td").get(40).text()));
            concurso.setAcumulado16Numeros(TypeUtils.parse(Double.class, tr.select("td").get(41).text()));
            concurso.setAcumulado0Numeros(TypeUtils.parse(Double.class, tr.select("td").get(42).text()));

            concurso.setEstimativaPremio(TypeUtils.parse(Double.class, tr.select("td").get(43).text()));
            concurso.setValorAcumuladoEspecial(TypeUtils.parse(Double.class, StringUtils.defaultIfBlank(tr.select("td").get(44).text(), "0")));

            Sorteio sorteio = new Sorteio(1);

            for (int i = 0; i < 20; i++) {
                int ordem = i + 1;
                int index = i + 2;
                sorteio.addRodada(ordem, tr.select("td").get(index).text());
            }

            Double rateio;
            Integer quantidade;

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(23).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(31).text());
            sorteio.addGanhador(rateio, 20, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(26).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(32).text());
            sorteio.addGanhador(rateio, 19, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(27).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(33).text());
            sorteio.addGanhador(rateio, 18, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(28).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(34).text());
            sorteio.addGanhador(rateio, 17, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(29).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(35).text());
            sorteio.addGanhador(rateio, 16, quantidade);

            quantidade = TypeUtils.parse(Integer.class, tr.select("td").get(30).text());
            rateio = TypeUtils.parse(Double.class, tr.select("td").get(36).text());
            sorteio.addGanhador(rateio, 0, quantidade);

            concurso.addSorteio(sorteio);
            concursos.add(concurso);

            row += rowspan;
        }

        return concursos;
    }
}
