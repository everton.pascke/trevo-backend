package br.com.elp.trevo.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractTask {

    @Autowired protected Environment env;

    protected boolean isDev() {
        List<String> profiles = Arrays.asList(env.getActiveProfiles());
        return profiles.contains("dev");
    }
}
