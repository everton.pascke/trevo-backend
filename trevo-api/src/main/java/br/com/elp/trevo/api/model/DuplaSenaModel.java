package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.DuplaSena;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DuplaSenaModel extends ConcursoModel {

    public Boolean acumuladoSenaSorteio1;
    public Double valorAcumuladoSenaSorteio1;
    public Double estimativaPremio;
    public Double acumuladoEspecialPascoa;

    public static DuplaSenaModel from(DuplaSena entity) {
        if (entity == null) {
            return null;
        }
        DuplaSenaModel model = new DuplaSenaModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.acumuladoSenaSorteio1 = entity.getAcumuladoSenaSorteio1();
        model.valorAcumuladoSenaSorteio1 = entity.getValorAcumuladoSenaSorteio1();
        model.estimativaPremio = entity.getEstimativaPremio();
        model.acumuladoEspecialPascoa = entity.getAcumuladoEspecialPascoa();
        return model;
    }

    public static List<DuplaSenaModel> from(List<DuplaSena> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
