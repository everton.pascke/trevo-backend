package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.DuplaSenaModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.DuplaSena;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("duplasena")
public class DuplaSenaController extends ConcursoController<DuplaSena, DuplaSenaModel> {

    public DuplaSenaController() {
        super(Concurso.Tipo.DUPLA_SENA);
    }

    @Override
    protected DuplaSenaModel parse(DuplaSena entity) {
        return DuplaSenaModel.from(entity);
    }

    @Override
    protected List<DuplaSenaModel> parse(List<DuplaSena> entities) {
        return DuplaSenaModel.from(entities);
    }
}
