package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.LotofacilModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.Lotofacil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("lotofacil")
public class LotofacilController extends ConcursoController<Lotofacil, LotofacilModel> {

    public LotofacilController() {
        super(Concurso.Tipo.LOTOFACIL);
    }

    @Override
    protected LotofacilModel parse(Lotofacil entity) {
        return LotofacilModel.from(entity);
    }

    @Override
    protected List<LotofacilModel> parse(List<Lotofacil> entities) {
        return LotofacilModel.from(entities);
    }
}
