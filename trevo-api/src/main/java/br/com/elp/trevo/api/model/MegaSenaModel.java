package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.MegaSena;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MegaSenaModel extends ConcursoModel {

    public Boolean acumulado;

    public Double valorAcumulado;
    public Double estimativaPremio;
    public Double acumuladoMegaVirada;

    public static MegaSenaModel from(MegaSena entity) {
        if (entity == null) {
            return null;
        }
        MegaSenaModel model = new MegaSenaModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.acumulado = entity.getAcumulado();
        model.valorAcumulado = entity.getValorAcumulado();
        model.estimativaPremio = entity.getEstimativaPremio();
        model.acumuladoMegaVirada = entity.getAcumuladoMegaVirada();
        return model;
    }

    public static List<MegaSenaModel> from(List<MegaSena> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
