package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Rodada;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RodadaModel {

    public Integer ordem;
    public String numero;

    public static RodadaModel from(Rodada entity) {
        if (entity == null) {
            return null;
        }
        RodadaModel model = new RodadaModel();
        model.ordem = entity.getOrdem();
        model.numero = entity.getNumero();
        return model;
    }

    public static List<RodadaModel> from(List<Rodada> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
