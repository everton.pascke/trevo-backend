package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Sorteio;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SorteioModel {

    public Integer ordem;
    public List<RodadaModel> rodadas;
    public List<GanhadorModel> ganhadores;

    public static SorteioModel from(Sorteio entity) {
        if (entity == null) {
            return null;
        }
        SorteioModel model = new SorteioModel();
        model.ordem = entity.getOrdem();
        model.rodadas = RodadaModel.from(entity.getRodadas());
        model.ganhadores = GanhadorModel.from(entity.getGanhadores());
        return model;
    }

    public static List<SorteioModel> from(List<Sorteio> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
