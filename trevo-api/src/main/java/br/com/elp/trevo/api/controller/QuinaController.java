package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.QuinaModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.Quina;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("quina")
public class QuinaController extends ConcursoController<Quina, QuinaModel> {

    public QuinaController() {
        super(Concurso.Tipo.QUINA);
    }

    @Override
    protected QuinaModel parse(Quina entity) {
        return QuinaModel.from(entity);
    }

    @Override
    protected List<QuinaModel> parse(List<Quina> entities) {
        return QuinaModel.from(entities);
    }
}
