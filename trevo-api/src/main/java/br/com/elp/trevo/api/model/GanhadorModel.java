package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Ganhador;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GanhadorModel {

    public Double rateio;
    public Integer acertos;
    public Integer quantidade;

    public static GanhadorModel from(Ganhador entity) {
        if (entity == null) {
            return null;
        }
        GanhadorModel model = new GanhadorModel();
        model.rateio = entity.getRateio();
        model.acertos = entity.getAcertos();
        model.quantidade = entity.getQuantidade();
        return model;
    }

    public static List<GanhadorModel> from(List<Ganhador> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
