package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Lotofacil;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LotofacilModel extends ConcursoModel {

    public Double estimativaPremio;
    public Double acumulado15Numeros;
    public Double valorAcumuladoEspecial;

    public static LotofacilModel from(Lotofacil entity) {
        if (entity == null) {
            return null;
        }
        LotofacilModel model = new LotofacilModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.estimativaPremio = entity.getEstimativaPremio();
        model.acumulado15Numeros = entity.getAcumulado15Numeros();
        model.valorAcumuladoEspecial = entity.getValorAcumuladoEspecial();
        return model;
    }

    public static List<LotofacilModel> from(List<Lotofacil> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
