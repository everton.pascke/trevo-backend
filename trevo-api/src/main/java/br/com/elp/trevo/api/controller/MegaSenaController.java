package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.MegaSenaModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.MegaSena;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("megasena")
public class MegaSenaController extends ConcursoController<MegaSena, MegaSenaModel> {

    public MegaSenaController() {
        super(Concurso.Tipo.MEGA_SENA);
    }

    @Override
    protected MegaSenaModel parse(MegaSena entity) {
        return MegaSenaModel.from(entity);
    }

    @Override
    protected List<MegaSenaModel> parse(List<MegaSena> entities) {
        return MegaSenaModel.from(entities);
    }
}
