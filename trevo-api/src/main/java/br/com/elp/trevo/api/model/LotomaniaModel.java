package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Lotomania;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LotomaniaModel extends ConcursoModel {

    public Double acumulado0Numeros;
    public Double acumulado16Numeros;
    public Double acumulado17Numeros;
    public Double acumulado18Numeros;
    public Double acumulado19Numeros;
    public Double acumulado20Numeros;

    public Double estimativaPremio;
    public Double valorAcumuladoEspecial;

    public static LotomaniaModel from(Lotomania entity) {
        if (entity == null) {
            return null;
        }
        LotomaniaModel model = new LotomaniaModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.acumulado0Numeros = entity.getAcumulado0Numeros();
        model.acumulado16Numeros = entity.getAcumulado16Numeros();
        model.acumulado17Numeros = entity.getAcumulado17Numeros();
        model.acumulado18Numeros = entity.getAcumulado18Numeros();
        model.acumulado19Numeros = entity.getAcumulado19Numeros();
        model.acumulado20Numeros = entity.getAcumulado20Numeros();
        model.estimativaPremio = entity.getEstimativaPremio();
        model.valorAcumuladoEspecial = entity.getValorAcumuladoEspecial();
        model.estimativaPremio = entity.getEstimativaPremio();
        return model;
    }

    public static List<LotomaniaModel> from(List<Lotomania> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
