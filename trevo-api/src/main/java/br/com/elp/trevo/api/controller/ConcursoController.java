package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.ConcursoModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.service.ConcursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public abstract class ConcursoController<T extends Concurso, M extends ConcursoModel> {

    @Autowired private ConcursoService concursoService;

    protected Concurso.Tipo tipo;

    public ConcursoController(Concurso.Tipo tipo) {
        this.tipo = tipo;
    }

    protected abstract M parse(T entity);
    protected abstract List<M> parse(List<T> entities);

    @GetMapping
    public ResponseEntity findLast() {
        T entity = (T) concursoService.findLastByTipo(tipo);
        if (entity != null) {
            M model = parse(entity);
            return ResponseEntity.ok(model);
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{numero}")
    public ResponseEntity findByNumero(@PathVariable Long numero) {
        T entity = (T) concursoService.findByTipoAndNumero(tipo, numero);
        if (entity != null) {
            M model = parse(entity);
            return ResponseEntity.ok(model);
        }
        return ResponseEntity.notFound().build();
    }
}
