package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Timemania;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TimemaniaModel extends ConcursoModel {

    public String timeCoracao;

    public Double valorAcumulado;
    public Double estimativaPremio;

    public static TimemaniaModel from(Timemania entity) {
        if (entity == null) {
            return null;
        }
        TimemaniaModel model = new TimemaniaModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.timeCoracao = entity.getTimeCoracao();
        model.valorAcumulado = entity.getValorAcumulado();
        model.estimativaPremio = entity.getEstimativaPremio();
        return model;
    }

    public static List<TimemaniaModel> from(List<Timemania> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
