package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.TimemaniaModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.Timemania;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("timemania")
public class TimemaniaController extends ConcursoController<Timemania, TimemaniaModel> {

    public TimemaniaController() {
        super(Concurso.Tipo.TIMEMANIA);
    }

    @Override
    protected TimemaniaModel parse(Timemania entity) {
        return TimemaniaModel.from(entity);
    }

    @Override
    protected List<TimemaniaModel> parse(List<Timemania> entities) {
        return TimemaniaModel.from(entities);
    }
}
