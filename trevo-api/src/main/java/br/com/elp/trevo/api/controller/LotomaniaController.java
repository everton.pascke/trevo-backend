package br.com.elp.trevo.api.controller;

import br.com.elp.trevo.api.model.LotomaniaModel;
import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.entity.Lotomania;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("lotomania")
public class LotomaniaController extends ConcursoController<Lotomania, LotomaniaModel> {

    public LotomaniaController() {
        super(Concurso.Tipo.LOTOMANIA);
    }

    @Override
    protected LotomaniaModel parse(Lotomania entity) {
        return LotomaniaModel.from(entity);
    }

    @Override
    protected List<LotomaniaModel> parse(List<Lotomania> entities) {
        return LotomaniaModel.from(entities);
    }
}
