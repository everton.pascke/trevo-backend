package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Quina;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class QuinaModel extends ConcursoModel {

    public Boolean acumulado;

    public Double valorAcumulado;
    public Double estimativaPremio;
    public Double acumuladoSorteioEspecialSaoJoao;

    public static QuinaModel from(Quina entity) {
        if (entity == null) {
            return null;
        }
        QuinaModel model = new QuinaModel();
        model.data = entity.getData();
        model.numero = entity.getNumero();
        model.arrecadacao = entity.getArrecadacao();
        model.regioes = RegiaoModel.from(entity.getRegioes());
        model.sorteios = SorteioModel.from(entity.getSorteios());
        model.acumulado = entity.getAcumulado();
        model.valorAcumulado = entity.getValorAcumulado();
        model.estimativaPremio = entity.getEstimativaPremio();
        model.acumuladoSorteioEspecialSaoJoao = entity.getAcumuladoSorteioEspecialSaoJoao();
        return model;
    }

    public static List<QuinaModel> from(List<Quina> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
