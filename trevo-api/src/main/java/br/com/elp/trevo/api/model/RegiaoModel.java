package br.com.elp.trevo.api.model;

import br.com.elp.trevo.persistence.entity.Regiao;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RegiaoModel {

    public String uf;
    public String cidade;

    public static RegiaoModel from(Regiao entity) {
        if (entity == null) {
            return null;
        }
        RegiaoModel model = new RegiaoModel();
        model.uf = entity.getUf();
        model.cidade = entity.getCidade();
        return model;
    }

    public static List<RegiaoModel> from(List<Regiao> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(entity -> from(entity))
                .collect(Collectors.toList());
    }
}
