package br.com.elp.trevo.api.model;

import java.util.Date;
import java.util.List;

public class ConcursoModel {

    public Date data;
    public Long numero;
    public Double arrecadacao;

    public List<RegiaoModel> regioes;
    public List<SorteioModel> sorteios;
}
