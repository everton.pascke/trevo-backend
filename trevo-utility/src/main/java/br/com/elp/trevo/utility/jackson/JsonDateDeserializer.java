package br.com.elp.trevo.utility.jackson;

import br.com.elp.trevo.utility.DateUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Date;

public class JsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        String text = parser.getText();
        if (StringUtils.isBlank(text)) {
            return null;
        }
        else {
            return DateUtils.parse(text);
        }
    }
}
