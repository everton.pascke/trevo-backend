package br.com.elp.trevo.utility;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public abstract class DateUtils extends Utils {

    public enum DateType {

        ISO_8601 ("yyyy-MM-dd'T'HH:mm:ss.SSSZ", "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[A-Z]{3}"),
        DATE_TIME ("yyyy-MM-dd HH:mm:ss", "\\d{4}-\\d{2}-\\d{2}"),
        DATE ("yyyy-MM-dd", "\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}"),

        DATE_TIME_BR ("dd/MM/yyyy HH:mm:ss", "\\d{2}/\\d{2}/\\d{4}\\s\\d{2}:\\d{2}:\\d{2}"),
        DATE_BR ("dd/MM/yyyy", "\\d{2}/\\d{2}/\\d{4}");

        private String pattern;
        private String expression;

        DateType(String pattern, String expression) {
            this.pattern = pattern;
            this.expression = expression;
        }

        public String getPattern() {
            return pattern;
        }

        public String getExpression() {
            return expression;
        }

        public static String[] getPatterns() {
            DateType[] types = values();
            String[] patterns = new String[types.length];
            for (int i = 0; i < types.length; i++) {
                patterns[i] = types[i].pattern;
            }
            return patterns;
        }

        public static String[] getExpressions() {
            DateType[] types = values();
            String[] expressions = new String[types.length];
            for (int i = 0; i < types.length; i++) {
                expressions[i] = types[i].expression;
            }
            return expressions;
        }
    }

    private static final String[] patterns = DateType.getPatterns();
    private static final String[] expressions = DateType.getExpressions();

    private static final DateType defaultType = DateType.ISO_8601;
    private static final String defaultPattern = defaultType.pattern;

    private static TimeZone getTimeZone() {
        TimeZone timeZone = TimeZone.getTimeZone("America/Sao_Paulo");
        return timeZone;
    }

    private static DateFormat getDateFormat(String pattern) {
        TimeZone timeZone = getTimeZone();
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, LOCALE);
        formatter.setTimeZone(timeZone);
        return formatter;
    }

    public static boolean isDate(String value) {
        if (StringUtils.isNotBlank(value)) {
            for (int i = 0; i < expressions.length; i++) {
                if (value.matches(expressions[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String format(long milliseconds) {
        Date date = new Date(milliseconds);
        return format(date);
    }

    public static String format(long milliseconds, DateType type) {
        Date date = new Date(milliseconds);
        return format(date, type);
    }

    public static String format(long milliseconds, String pattern) {
        Date date = new Date(milliseconds);
        return format(date, pattern);
    }

    public static String format(Date value) {
        return format(value, defaultType.getPattern());
    }

    public static String format(Date value, DateType type) {
        return format(value, type.pattern);
    }

    public static String format(Date value, String pattern) {
        if (value != null) {
            try {
                DateFormat formatter = getDateFormat(pattern);
                return formatter.format(value);
            } catch (Exception e) {

            }
        }
        return null;
    }

    public static Date parse(long time) {
        TimeZone timeZone = getTimeZone();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(timeZone);
        calendar.setTimeInMillis(time);
        return calendar.getTime();
    }

    public static Date parse(String value) {
        return parse(value, patterns);
    }

    public static Date parse(String value, String... patterns) {
        if (StringUtils.isNotBlank(value)) {
            DateFormat formatter;
            for (String pattern : patterns) {
                formatter = getDateFormat(pattern);
                try {
                    return formatter.parse(value);
                } catch (ParseException e) {
                    // try next ...
                }
            }
        }
        return null;
    }

    public static boolean isBetween(Date minDay, Date maxDay, Date day) {
        return day.compareTo(minDay) >= 0 && day.compareTo(maxDay) <= 0;
    }

    public static Date getActualMinDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        truncate(cal);
        return cal.getTime();
    }

    public static Date getActualMaxDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        truncate(cal);
        return cal.getTime();
    }

    public static Date getActualMinDay(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date date = cal.getTime();
        return getFirstTimeOfDay(date);
    }

    public static Date getActualMaxDay(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date date = cal.getTime();
        return getLastTimeOfDay(date);
    }

    public static int getYear(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        int ano = cal.get(Calendar.YEAR);
        return ano;
    }

    public static int getDay(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        int ano = cal.get(Calendar.DAY_OF_MONTH);
        return ano;
    }

    public static int getMonth(Date day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        int ano = cal.get(Calendar.DAY_OF_MONTH);
        return ano;
    }

    public static long getDaysBetween(Date dateStart, Date dateEnd) {
        long diff = dateEnd.getTime() - dateStart.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static void truncate(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static boolean equals(Date date1, Date date2, boolean truncate) {
        Date d1 = date1;
        Date d2 = date2;
        if (truncate) {
            d1 = truncate(date1);
            d2 = truncate(date2);
        }
        return d1.equals(d2);
    }

    public static Date truncate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        truncate(calendar);
        return calendar.getTime();
    }

    public static Date getLastTimeOfDay() {
        return getLastTimeOfDay(new Date());
    }

    public static Date getFirstTimeOfDay() {
        return getFirstTimeOfDay(new Date());
    }

    public static Date getFirstTimeOfDay(Date date) {
        return truncate(date);
    }

    public static Date getLastTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }
}
