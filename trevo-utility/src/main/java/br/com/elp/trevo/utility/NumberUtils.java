package br.com.elp.trevo.utility;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

public abstract class NumberUtils extends Utils {
	
	public static String format(Number value) {
		return format(value, DECIMAL);
	}
	
	public static String format(Number value, String pattern) {
		if (value != null) {
			try {
				NumberFormat formatter = new DecimalFormat(pattern, new DecimalFormatSymbols(LOCALE));
				return formatter.format(value);
			} catch (Exception e) {
				
			}
		}
		return null;
	}
	
	public static Double parseDouble(String value) {
		return parseDouble(value, (Double) null);
	}
	
	public static Double parseDouble(String value, String pattern) {
		return parseDouble(value, pattern, null);
	}
	
	public static Double parseDouble(String value, Double defaultValue) {
		return parseDouble(value, DECIMAL, defaultValue);
	}
	
	public static Double parseDouble(String value, String pattern, Double defaultValue) {
		if (StringUtils.isNotBlank(value)) {
			try {
				if (!value.contains(",")) {
					return Double.valueOf(value);
				}
				else {
					NumberFormat formatter = new DecimalFormat(pattern, new DecimalFormatSymbols(LOCALE));
					Number number = formatter.parse(value);
					if (number != null) {
						return number.doubleValue();
					}
				}
			} catch (Exception e) {
				
			}
		}
		return defaultValue;
	}
}
