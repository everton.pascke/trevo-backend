package br.com.elp.trevo.utility;

import java.util.Locale;

public abstract class Utils {

    public static final Locale LOCALE = new Locale("pt", "BR");
    public static final String DECIMAL = "#,##0.00";
}
