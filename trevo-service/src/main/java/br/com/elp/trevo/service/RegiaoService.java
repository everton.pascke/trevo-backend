package br.com.elp.trevo.service;

import br.com.elp.trevo.persistence.entity.Regiao;
import br.com.elp.trevo.persistence.repository.RegiaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegiaoService {

    @Autowired private RegiaoRepository regiaoRepository;

    @Transactional(rollbackFor = Throwable.class)
    public void save(Regiao regiao) {
        regiaoRepository.save(regiao);
    }
}
