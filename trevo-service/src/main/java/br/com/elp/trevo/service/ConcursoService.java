package br.com.elp.trevo.service;

import br.com.elp.trevo.persistence.entity.Concurso;
import br.com.elp.trevo.persistence.repository.ConcursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConcursoService {

    @Autowired private ConcursoRepository concursoRepository;

    public Concurso findLastByTipo(Concurso.Tipo tipo) {
        return concursoRepository.findFirstByTipoOrderByNumeroDesc(tipo);
    }

    public Concurso findByTipoAndNumero(Concurso.Tipo tipo, Long numero) {
        return concursoRepository.findByTipoAndNumero(tipo, numero);
    }
}
