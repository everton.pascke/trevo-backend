package br.com.elp.trevo.service;

import br.com.elp.trevo.persistence.entity.Rodada;
import br.com.elp.trevo.persistence.repository.RodadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RodadaService {

    @Autowired private RodadaRepository rodadaRepository;

    @Transactional(rollbackFor = Throwable.class)
    public void save(Rodada rodada) {
        rodadaRepository.save(rodada);
    }
}
