package br.com.elp.trevo.service;

import br.com.elp.trevo.persistence.entity.Sorteio;
import br.com.elp.trevo.persistence.repository.SorteioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SorteioService {

    @Autowired private SorteioRepository sorteioRepository;

    @Transactional(rollbackFor = Throwable.class)
    public void save(Sorteio sorteio) {
        sorteioRepository.save(sorteio);
    }
}
